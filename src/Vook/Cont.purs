-----------------------------------------------------------------------------
-- |
-- Continuation monad helpers.
-- 
-- Roughly implementations of haskell package \'failible\'
-- (<https://github.com/matsubara0507/fallible>).
-----------------------------------------------------------------------------

module Vook.Cont where

import Prelude

import Control.Monad.Cont.Trans (ContT(..), runContT)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))


orLeftWith :: forall f e a. Applicative f => (Either e a) -> (e -> f a) -> f a
orLeftWith (Left e) f = f e
orLeftWith (Right a)_ = pure a

orNothingWith :: forall f a. Applicative f => Maybe a -> f a -> f a
orNothingWith Nothing x = x
orNothingWith (Just a) _ = pure a

orFalseWith :: forall f. Applicative f => Boolean -> f Unit -> f Unit
orFalseWith false x = x
orFalseWith true _ = pure unit

orLeftWithM :: forall m e a. Monad m => m (Either e a) -> (e -> m a) -> m a
orLeftWithM r k = r >>= (_ `orLeftWith` k)
orNothingWithM :: forall m a. Monad m => m (Maybe a) -> m a -> m a
orNothingWithM r k = r >>= (_ `orNothingWith` k)
orFalseWithM :: forall m. Monad m => m Boolean -> m Unit -> m Unit
orFalseWithM r k = r >>= (_ `orFalseWith` k)



evalContT :: forall m r. Monad m => ContT r m r -> m r
evalContT m = runContT m pure

term :: forall r m a. m r -> ContT r m a
term = ContT <<< const

termA :: forall r f a. Applicative f => r -> ContT r f a
termA = term <<< pure

terminate :: forall f a. Applicative f => ContT Unit f a
terminate = termA unit

