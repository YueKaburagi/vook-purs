
module Vook.TagQuery.Parser ( parser
                            , tqTag, tqTagS, tqUri, tqText, tqTextS
                            , tqNot, tqAll, tqAny
                            ) where

import Prelude

import Control.Alt ((<|>))
import Control.Monad.Rec.Class (class MonadRec, Step(..), tailRecM)
import Data.Array as Array
import Data.Bifunctor (bimap)
import Data.Foldable (class Foldable, foldMap, foldl)
import Data.Lazy (defer, force, Lazy)
import Data.List (List(..), (:), reverse)
import Data.List.NonEmpty (NonEmptyList(..))
import Data.List.NonEmpty as NEL
import Data.NonEmpty ((:|))
import Data.String.CodeUnits as Str
import Text.Parsing.StringParser (Parser, fail, try)
import Text.Parsing.StringParser.CodePoints (eof, string, char, oneOf, noneOf, satisfy, anyChar)
import Text.Parsing.StringParser.Combinators (many, many1, manyTill, many1Till, choice, withError, lookAhead)

import Vook.Types (TagQuery(..))


parser :: Parser TagQuery
parser = many whitespace *> force tagQuery <* many whitespace

toStr :: forall f. Foldable f => f Char -> String
toStr = foldMap Str.singleton

cws :: Array Char
cws = [' ', '\t', '\r', '\n', '　']

whitespace :: Parser Char
whitespace = oneOf cws
validChar :: Parser Char
validChar = noneOf cws
validStrHead :: Parser Char
validStrHead = noneOf (cws <> [')'])

quoteDelimitedStr :: Parser String
quoteDelimitedStr = toStr <$> (char '"' *> manyTill (satisfy (\c -> c /= '"')) (char '"'))
str :: Parser String
str = do
  h <- validStrHead
  rs <- manyTill validChar (lookAhead (void whitespace <|> eof))
  pure $ toStr (h : rs)

name :: Parser String
name = quoteDelimitedStr <|> str

semicolonDelimitedStr :: Parser String
semicolonDelimitedStr = toStr <$> many1 (noneOf (cws <> [';']))

semicolonDelimitedName :: Parser String
semicolonDelimitedName = quoteDelimitedStr <|> semicolonDelimitedStr 
colonDelimitedStr :: Parser String
colonDelimitedStr = toStr <$> many1 (noneOf (cws <> [':']))
colonDelimitedName :: Parser String
colonDelimitedName = quoteDelimitedStr <|> colonDelimitedStr

cdName = colonDelimitedName
scdName = semicolonDelimitedName


colon :: Parser Char
colon = oneOf [':', '：']


tagQuery :: Lazy (Parser TagQuery)
tagQuery = defer \_ ->
  choice
  [ tqNot
  , try tqTagS, try tqTag, try tqUri
  , tqAll, tqAny
  , try tqTextS, tqText
  ]

tqTag :: Parser TagQuery
tqTag =
  Tag <$> (
    try (char 't' *> colon *> name)
    <|> (cdName <* colon <* char 't')
    )

tqTagS :: Parser TagQuery
tqTagS =
  TagStrict <$> (
    try (char 't' *> colon *> scdName <* string ";")
    <|> (cdName <* colon <* string "t;")
    )

tqUri :: Parser TagQuery
tqUri =
  Url <$> (char 'u' *> colon *> str)

tqText :: Parser TagQuery
tqText =
  AnyText <$> name

tqTextS :: Parser TagQuery
tqTextS =
  AnyTextStrict <$> (scdName <* char ';')


tqNot :: Parser TagQuery
tqNot = do
  void $ char '!'
  t <- force tagQuery
  pure $ Not t
-- delayed:
--  Not <$> (char '!' *> force tagQuery)

tqAll :: Parser TagQuery
tqAll = do
  void $ string "(&"
  void $ many1 whitespace
  ts <- many (force tagQuery <* many1 whitespace)
  void $ char ')'
  pure $ (All <<< Array.fromFoldable) ts
-- delayed:
--  (All <<< Array.fromFoldable)
--  <$> (string "(&" *> many1 whitespace *> many (force tagQuery <* many1 whitespace) <* char ')')

tqAny :: Parser TagQuery
tqAny = do
  void $ string "(|"
  void $ many1 whitespace
  ts <- many (force tagQuery <* many1 whitespace)
  void $ char ')'
  pure $ (Any <<< Array.fromFoldable) ts
-- delayed:
--  (Any <<< Array.fromFoldable)
--  <$> (string "(|" *> many1 whitespace *> many (force tagQuery <* many1 whitespace) <* char ')')


