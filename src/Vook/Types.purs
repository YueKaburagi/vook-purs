module Vook.Types where

import Prim hiding (Record)
import Prelude

import Control.Alt (class Alt, (<|>))
import Data.Argonaut.Core (jsonEmptyObject)
import Data.Argonaut.Decode.Class (class DecodeJson, decodeJson)
import Data.Argonaut.Decode.Combinators ((.:), (.:?), (.!=))
import Data.Argonaut.Encode.Class (class EncodeJson)
import Data.Argonaut.Encode.Combinators ((:=), (~>))
import Data.Default (class Default)
import Data.Either (Either(..))
import Data.Foldable (class Foldable, foldl)
import Data.Newtype (class Newtype)
import Data.NonEmpty (NonEmpty, foldl1, (:|))
import Data.List.Types (List)
import Data.Maybe (Maybe(..))
import Data.Set (Set)
import Data.Tuple (Tuple)
import Halogen.Query.HalogenM (SubscriptionId)
import Web.Event.Event (Event)
import Web.UIEvent.MouseEvent (MouseEvent)


data SortType = Randomize | ByRate

data InputName = INSearch
               | INArticleTag

data Action = FireInput InputName Event
            | SortBy SortType
            | SetTagSearchMenu String MouseEvent
            | CloseTagSearchMenu
            | SetArticleMenu Article
            | ArticleAddTag Article String
            | SetQuery TagQuery
            | AddQuery TagQuery
            | FetchAll
            | FetchAllArticles
            | FetchAllPrisets
            | IntermittentFilterApplying Int TagQuery (Array Article) (Array Article)
            | FinishFilterApplying


data VookResponse a = VRFailed String
                    | VRSucceed a
instance decodeJSONVookResponse :: DecodeJson a => DecodeJson (VookResponse a) where
  decodeJson json = do
    x <- decodeJson json
    oneOf $ vrF x :| [vrS x]
    where
      vrF v = VRFailed <$> v .: "failed"
      vrS v = VRSucceed <$> v .: "succeed"
vookResponseToEither :: forall a. VookResponse a -> Either String a
vookResponseToEither =
  case _ of
    VRFailed s -> Left s
    VRSucceed a -> Right a

type URI = String

newtype Record = Record
                 { name :: String
                 , uri :: URI
                 , tags :: Set String
                 , comment :: Maybe String
                 , thumbnail :: Array String
                 , index :: String
                 }
derive instance newtypeRecord :: Newtype Record _
instance decodeJSONRecord :: DecodeJson Record where
  decodeJson json = do
    x <- decodeJson json
    name <- x .: "name"
    uri <- x .: "url"
    tags <- x .:? "tags" .!= mempty
    comment <- x .:? "comment"
    thumbnail <- x .:? "thumbnail" .!= mempty
    index <- x .: "index"
    pure $ Record { name, uri, tags, comment, thumbnail, index }

newtype Article = Article
                  { title :: String
                  , records :: Array Record
                  , tags :: Set String
                  , comment :: Maybe String
                  , index :: String
                  }
derive instance newtypeArticle :: Newtype Article _
instance decodeJSONArticle :: DecodeJson Article where
  decodeJson json = do
    x <- decodeJson json
    title <- x .: "title"
    records <- x .: "records"
    tags <- x .:? "tags" .!= mempty
    comment <- x .:? "comment"
    index <- x .: "index"
    pure $ Article { title, records, tags, comment, index }

type TagSearchMenu = { target :: String
                     , lifecount :: Int
                     , pos :: Tuple Int Int
                     }
type ArticleMenu =
  { article :: Article
  , tag :: String
  }

type ErrorString = String

oneOf :: forall f m a. Foldable f => Alt m => NonEmpty f (m a) -> m a
oneOf = foldl1 (<|>)

data TagQuery = Tag String
              | TagStrict String
              | Url String
              | AnyText String
              | AnyTextStrict String
              | Not TagQuery
              | All (Array TagQuery)
              | Any (Array TagQuery)
derive instance eqTagQuery :: Eq TagQuery
derive instance ordTagQuery :: Ord TagQuery
instance decodeJSONTagQuery :: DecodeJson TagQuery where
  decodeJson json = do
    x <- decodeJson json
    oneOf $ oTag x :| [oTagS x, oUrl x, oText x, oTextS x, oNot x, oAll x, oAny x]
    where
      oTag x = Tag <$> x .: "tag_partial"
      oTagS x = TagStrict <$> x .: "tag"
      oUrl x = Url <$> x .: "uri"
      oText x = AnyText <$> x .: "text"
      oTextS x = AnyTextStrict <$> x .: "text_strict"
      oNot x = Not <$> x .: "not"
      oAll x = All <$> x .: "all"
      oAny x = Any <$> x .: "any"
instance encodeJSONTagQuery :: EncodeJson TagQuery where
  encodeJson =
    case _ of
      Tag tag ->
        "tag_partial" := tag ~> jsonEmptyObject
      TagStrict tag ->
        "tag" := tag ~> jsonEmptyObject
      Url uri ->
        "uri" := uri ~> jsonEmptyObject
      AnyText str ->
        "text" := str ~> jsonEmptyObject
      AnyTextStrict str ->
        "text_strict" := str ~> jsonEmptyObject
      Not q ->
        "not" := q ~> jsonEmptyObject
      All l ->
        "all" := l ~> jsonEmptyObject
      Any l ->
        "any" := l ~> jsonEmptyObject
instance showTagQuery :: Show TagQuery where
  show =
    case _ of
      Tag tag -> "Tag(" <> tag <> ")"
      TagStrict tag -> "TagS( " <> tag <> ")"
      Url url -> "Url(" <> url <> ")"
      AnyText str -> "Text(" <> str <> ")"
      AnyTextStrict str -> "TextS(" <> str <> ")"
      Not q -> "Not(" <> show q <> ")"
      All qs -> "All( " <> show qs <> " )"
      Any qs -> "Any( " <> show qs <> " )"

tagQueryToPlainString :: TagQuery -> String
tagQueryToPlainString =
  case _ of
    Tag tag -> "t:" <> tag
    TagStrict tag -> "t:" <> tag <> ";"
    Url url -> "u:" <> url
    AnyText str -> str
    AnyTextStrict str -> str <> ";"
    Not q -> "!" <> tagQueryToPlainString q
    All qs -> "(& " <> allet qs <> " )"
    Any qs -> "(| " <> allet qs <> " )"
  where
    allet = foldl (\s x -> s <> " " <> tagQueryToPlainString x) ""

newtype Priset = Priset
                 { name :: String
                 , query :: TagQuery
                 , index :: String
                 }
instance decodeJSONPriset :: DecodeJson Priset where
  decodeJson json = do
    x <- decodeJson json
    name <- x .: "name"
    query <- x .: "query"
    index <- x .: "index"
    pure $ Priset { name, query, index }

newtype State = State
              { queryStr :: String
              , articles :: Array Article
              , visibles :: Array Article
              , prisets :: Array Priset
              , query :: Maybe TagQuery
              , menu :: Maybe TagSearchMenu
              , errLog :: List ErrorString
              , filterApplyingAt :: Maybe SubscriptionId
              , tagSerchMenuDisplayingAt :: Maybe SubscriptionId
              , articleMenu :: Maybe ArticleMenu
              }
derive instance newtypeState :: Newtype State _
instance defaultState :: Default State where
  def = State
        { queryStr : ""
        , articles : mempty
        , visibles : mempty
        , prisets : mempty
        , query : Nothing
        , menu : Nothing
        , errLog : mempty
        , filterApplyingAt : Nothing
        , articleMenu : Nothing
        , tagSerchMenuDisplayingAt : Nothing
        }

