
module Vook.TagQuery (toFilter) where

import Prelude

import Data.Foldable (all, any)
import Data.Newtype (wrap)
import Data.String (length, contains)
import Data.String as String

import Vook.Types (TagQuery(..), Article(..), Record(..))

-- unicodeの平仮名領域の 3041-3094 と 片仮名領域の 30A1-30F4 は同じ順で並んでいる
katakanaAsHiragana :: Char -> Char
katakanaAsHiragana = identity

expand :: String -> String
expand = String.toLower

partialN :: Int -> String -> String -> Boolean
partialN limit key target
  | limit >= length key = true
  | otherwise = contains (wrap key) (expand target)

toFilter :: TagQuery -> Article -> Boolean
toFilter tq article@(Article a) =
  case tq of
    Tag tag ->
      any (partial tag) a.tags || any (\(Record r) -> any (partial tag) r.tags) a.records
    TagStrict tag ->
      any (strict tag) a.tags || any (\(Record r) -> any (strict tag) r.tags) a.records
    Url url ->
      any (\(Record r) -> partial url r.uri) a.records
    AnyText str ->
      partial str a.title || any (\(Record r) -> partial str r.name) a.records
    AnyTextStrict str ->
      strict str a.title || any (\(Record r) -> strict str r.name) a.records
    Not t ->
      not (toFilter t article)
    All l ->
      all (flip toFilter article) l
    Any l ->
      any (flip toFilter article) l
  where
    partial = partialN 2
    strict x y = x == y

