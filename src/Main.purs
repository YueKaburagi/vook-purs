module Main where

import Prim hiding (Record)
import Prelude

import Control.Monad.Cont.Trans (ContT, lift)
import Control.Monad.State.Class (modify_, gets)
import Data.Argonaut.Core (Json)
import Data.Argonaut.Decode.Class (class DecodeJson, decodeJson)
import Data.Argonaut.Encode.Class (encodeJson)
import Data.Array as Array
import Data.Default (def)
import Data.Either (Either)
import Data.Foldable (null)
import Data.Newtype (wrap, over, un)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.List.Types ((:))
import Data.List as List
import Data.Set as Set
import Data.String (length)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Class (liftEffect)
import Effect.Aff (Aff)
import Effect.Aff as Aff
import Effect.Aff.Class (class MonadAff, liftAff)
import Foreign.Object as FO
import Halogen as H
import Halogen (Component)
import Halogen.Aff as HAff
import Halogen.HTML as HH
import Halogen.HTML (HTML)
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HQ
import Halogen.VDom.Driver (runUI)
import DOM.HTML.Indexed.InputType (InputType(InputText))
import Text.Parsing.StringParser (runParser)
import Web.Event.Event (stopPropagation)
import Web.Event.Event as Event
import Web.HTML.HTMLInputElement as InputElement
import Web.UIEvent.MouseEvent as MouseEvent
import Affjax as AX
import Affjax.ResponseFormat (json)
import Affjax.RequestBody as AXQ

import Vook.Types (SortType(..), Action(..), Record(..), Article(..), Priset(..), State(..), TagQuery(..), tagQueryToPlainString, ErrorString, TagSearchMenu(..), ArticleMenu(..), vookResponseToEither, InputName(..))
import Vook.TagQuery (toFilter)
import Vook.TagQuery.Parser (parser)
import Vook.Cont (orLeftWith, orLeftWithM, orNothingWith, orNothingWithM, orFalseWith, terminate, term, evalContT)

main :: Effect Unit
main = HAff.runHalogenAff do
  body <- HAff.awaitBody
  runUI component unit body


component :: forall query input output. Component HTML query input output Aff
component =
  H.mkComponent
    { initialState : initState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction, initialize = Just FetchAll }
    }

initState :: forall input. input -> State
initState _ = def

removeMenu :: forall a o m. H.HalogenM State a () o m Unit
removeMenu = do
  ix <- H.gets $ un State >>> _.tagSerchMenuDisplayingAt
  maybe (pure unit) H.unsubscribe ix
  H.modify_ $ over State $ _ { menu = Nothing, tagSerchMenuDisplayingAt = Nothing }
setQuery :: TagQuery -> State -> State
setQuery q = over State $ _ { query = Just q, queryStr = tagQueryToPlainString q }
pushErrorString :: String -> State -> State
pushErrorString str = over State $ \x -> x { errLog = str : x.errLog }

ezHandleError :: forall a o m x. MonadAff m => DecodeJson a
                 => H.HalogenM State Action () o m (Either AX.Error ({body :: Json | x}))
                 -> ContT Unit (H.HalogenM State Action () o m) a
ezHandleError g = do
  r <- lift g
  d <- r `orLeftWith` (\err -> term $ H.modify_ $ pushErrorString (AX.printError err))
  decodeJson d.body `orLeftWith` (\err -> term $ H.modify_ $ pushErrorString (show err))

countdown :: Int -> TagSearchMenu -> TagSearchMenu
countdown x = _ { lifecount = x - 1 }

replaceById :: Article -> Array Article -> Array Article
replaceById t@(Article a) = map (\s@(Article x) -> if x.index == a.index then t else s)

handleAction :: forall o m. MonadAff m => Action -> H.HalogenM State Action () o m Unit
handleAction = 
  case _ of
    FireInput INSearch e ->
      evalContT $ do
        ie <- (InputElement.fromEventTarget =<< Event.target e) `orNothingWith` terminate
        str <- liftEffect $ InputElement.value ie
        modify_ $ over State $ _ { queryStr = str }
        (length str > 2) `orFalseWith` terminate
        tq <- runParser parser str `orLeftWith` const terminate
        otq <- gets $ un State >>> _.query
        (otq /= Just tq) `orFalseWith` terminate
        modify_ $ over State $ _ { query = Just tq } -- do not overwrite queryStr
        lift $ startFilterApply
    FireInput INArticleTag e ->
      evalContT $ do
        ie <- (InputElement.fromEventTarget =<< Event.target e) `orNothingWith` terminate
        str <- lift $ liftEffect $ InputElement.value ie
        modify_ $ over State $ \x -> x { articleMenu = (_ { tag = str } ) <$> x.articleMenu }
    SortBy _ -> pure unit
    SetTagSearchMenu tag me -> do
      liftEffect $ stopPropagation $ MouseEvent.toEvent me
      oix <- gets $ un State >>> _.tagSerchMenuDisplayingAt
      maybe (pure unit) H.unsubscribe oix
      modify_ $ over State $ _ { menu = Just { target : tag, lifecount : 100, pos : Tuple 100 100 } }
      ix <- H.subscribe nextTimer
      modify_ $ over State $ _ { tagSerchMenuDisplayingAt = Just ix }
    CloseTagSearchMenu -> do
      lives <- gets $ un State >>> _.menu >>> maybe 0 _.lifecount
      if lives <= 0
        then removeMenu
        else do
        modify_ $ over State $ \x -> x { menu = countdown lives <$> x.menu }
        ix <- H.subscribe nextTimer
        modify_ $ over State $ _ { tagSerchMenuDisplayingAt = Just ix }
    SetArticleMenu a -> do
      modify_ $ over State $ _ { articleMenu = Just { article : a, tag : "" } }
    ArticleAddTag (Article a) tag ->
      evalContT $ do
        vr <- ezHandleError $ liftAff $ AX.patch json "http://localhost:1357/article/add/tag" (AXQ.json $ encodeJson { index : a.index, tag } )
        na <- vookResponseToEither vr `orLeftWith` (\x -> term $ modify_ $ pushErrorString x)
        modify_ $ over State $ \x -> x { articles = replaceById na x.articles
                                       , visibles = replaceById na x.visibles }
        modify_ $ over State $ _ { articleMenu = Nothing }
    SetQuery tq -> do
      removeMenu
      modify_ $ setQuery tq
      startFilterApply
    AddQuery tq -> do
      otq <- gets $ un State >>> _.query
      let order =
            case otq of
              Nothing -> tq
              Just old ->
                case old of
                  All l -> All $ [tq] <> l
                  _ -> All [tq, old]
      removeMenu
      modify_ $ setQuery order
      startFilterApply
    FetchAll ->
      evalContT $ do
        prisets <- ezHandleError $ liftAff $ AX.get json "http://localhost:1357/prisets"
        modify_ $ over State $ _ { prisets = prisets }
        articles <- ezHandleError $ liftAff $ AX.get json "http://localhost:1357/entries"
        modify_ $ over State $ _ { articles = articles }
    FetchAllArticles -> do
      evalContT $ do
        articles <- ezHandleError $ liftAff $ AX.get json "http://localhost:1357/entries"
        modify_ $ over State $ _ { articles = articles }
    FetchAllPrisets -> do
      evalContT $ do
        arr <- ezHandleError $ liftAff $ AX.get json "http://localhost:1357/prisets"
        modify_ $ over State $ _ { prisets = arr }
    IntermittentFilterApplying width tq rest rs -> do
      modify_ $ over State $ \s -> s { visibles = s.visibles <> rs }
      sid <- H.subscribe (intermittentFilterApply width tq rest)
      modify_ $ over State $ _ { filterApplyingAt = Just sid }
    FinishFilterApplying -> do
      modify_ $ over State $ _ { filterApplyingAt = Nothing }


startFilterApply :: forall o m. MonadAff m => H.HalogenM State Action () o m Unit
startFilterApply =
  evalContT $ do
    osid <- gets $ un State >>> _.filterApplyingAt
    lift $ maybe (pure unit) H.unsubscribe osid
    query <- (gets $ un State >>> _.query) `orNothingWithM` terminate
    modify_ $ over State $ _ { visibles = (mempty :: Array Article) }
    arts <- gets $ un State >>> _.articles
    sid <- lift $ H.subscribe (intermittentFilterApply 250 query arts)
    modify_ $ over State $ _ { filterApplyingAt = Just sid }


nextTimer :: forall m. MonadAff m => HQ.EventSource m Action
nextTimer =
  HQ.affEventSource \emitter -> do
    Aff.delay $ wrap 50.0
    HQ.emit emitter CloseTagSearchMenu
    pure mempty

intermittentFilterApply :: forall m. MonadAff m
                        => Int -> TagQuery -> Array Article -> HQ.EventSource m Action
intermittentFilterApply width tq qs =
  HQ.affEventSource \emitter -> do
    let rest = Array.drop width qs
    let rs = Array.filter (toFilter tq) (Array.take width qs)
    Aff.delay (wrap 18.0)
    if null rest then
      HQ.emit emitter FinishFilterApplying
    else
      HQ.emit emitter $ IntermittentFilterApplying width tq rest rs
    pure mempty


class_ :: forall r i. String -> HP.IProp (class :: String | r) i
class_ str = HP.class_ $ wrap str

render :: forall m. State -> H.ComponentHTML Action () m
render (State s) =
  HH.div_
  [ HH.div [ class_ "priset_buttons" ] ( map drawPriset s.prisets )
  , HH.div_
    [ HH.input
      [ HP.type_ InputText
      , HP.autocomplete false
      , HP.attr (wrap "size") "128"
      , HP.id_ "search"
      , HP.value s.queryStr
      , HE.onInput $ Just <<< FireInput INSearch
      ]
    , HH.button [ HP.tabIndex (-1), HE.onClick \_ -> Just $ SortBy Randomize ] [ HH.text "Random" ]
    , HH.button [ HP.tabIndex (-1), HE.onClick \_ -> Just $ SortBy ByRate ] [ HH.text "ByRate" ]
    ]
  , HH.hr_
  , HH.div [ class_ "open-blank" ]
    [ HH.a [ class_ "open-blank"
           , HP.href "about:blank"
           , HP.target "_blank"
           , HP.tabIndex (-1)
           ] [ HH.text "---------- open-blank-page ----------" ]
    ]
  , HH.hr_
  , HH.div [ class_ "error-log" ] ( map drawErrorString $ List.toUnfoldable s.errLog )
  , HH.section [ HP.id_ "autoins" ] ( map drawArticle s.visibles )
  , maybe drawNothing drawTagMenu s.menu
  , maybe drawNothing drawArticleMenu s.articleMenu
  , HH.span [ class_ "lesser" ] [ HH.text "vook" ]
  ]


mockElement :: forall w i . HTML w i
mockElement = HH.text "mock"
drawErrorString :: forall w i. ErrorString -> HTML w i
drawErrorString str = HH.pre_ [ HH.text str ]

drawRecord :: forall w i. Record -> HTML w i
drawRecord (Record r) =
  HH.a [ HP.href r.uri, HP.target "_blank" ] [ HH.text r.name ]

drawTag :: forall w. String -> HTML w Action
drawTag tag =
  HH.span [ class_ "tag"
          , HE.onClick \e -> Just $ SetTagSearchMenu tag e
          ] [ HH.text tag ]

drawNothing :: forall w i. HTML w i
drawNothing =
  HH.span [ HP.attr (wrap "style") "display: none;" ] []

drawComment :: forall w i. String -> HTML w i
drawComment comment =
  HH.span [ class_ "lesser" ] [ HH.text comment ]

drawArticle :: forall w. Article -> HTML w Action
drawArticle t@(Article a) =
  HH.article_
  [ HH.div [ class_ "title-line", HE.onClick \_ -> Just $ SetArticleMenu t ] [ HH.text a.title ]
  , HH.span [ class_ "records" ] ( map drawRecord a.records )
  , HH.span [ class_ "tags" ] ( map drawTag $ Set.toUnfoldable a.tags )
  , fromMaybe drawNothing $ map drawComment a.comment
  ]

drawPriset :: forall w. Priset -> HTML w Action
drawPriset (Priset p) =
  HH.button [ class_ "priset"
            , HE.onClick \_ -> Just $ SetQuery p.query
            ] [ HH.text p.name ]


drawTagMenu :: forall w. TagSearchMenu -> HTML w Action
drawTagMenu menu =
  HH.div [ class_ "popup-tag-menu" ]
  [ HH.span [ class_ "popup-tag-item"
            , HE.onClick \_ -> Just $ AddQuery $ TagStrict menu.target
            ] [ HH.text ("Add " <> menu.target) ]
  , HH.span [ class_ "popup-tag-item"
            , HE.onClick \_ -> Just $ AddQuery $ Not $ TagStrict menu.target
            ] [ HH.text ("Add Not " <> menu.target) ]
  , HH.span [ class_ "popup-tag-item"
            , HE.onClick \_ -> Just $ SetQuery $ TagStrict menu.target
            ] [ HH.text ("Set " <> menu.target) ]
  ]


drawArticleMenu :: forall w. ArticleMenu -> HTML w Action
drawArticleMenu menu =
  HH.div [ class_ "popup-article-menu" ]
  [ HH.span [ class_ "article-name" ] [ HH.text $ menu.article # un Article >>> _.title ]
  , HH.input [ HP.type_ InputText
             , HP.id_ "tag-name"
             , HE.onInput $ Just <<< FireInput INArticleTag
             ]
  , HH.button [ HE.onClick \_ -> Just $ ArticleAddTag menu.article menu.tag ] [ HH.text "Add" ]
  ]
