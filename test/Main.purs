module Test.Main where

import Prelude

import Data.Either (Either(..))
import Text.Parsing.StringParser (runParser)
import Vook.Types (TagQuery(..))
import Vook.TagQuery.Parser (parser)
import Vook.TagQuery.Parser as P

import Effect (Effect)
import Effect.Aff (launchAff_)
import Test.Spec (pending, describe, it)
import Test.Spec.Assertions (shouldEqual, shouldNotEqual)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (runSpec)

main :: Effect Unit
main = launchAff_ $ runSpec [consoleReporter] do
  describe "vook" do
    describe "parse query" do
      describe "unit k=1" do
        it "tag prefix" $
          runParser P.tqTag "t:abc" `shouldEqual` Right (Tag "abc")
        it "tag suffix" $
          runParser P.tqTag "abc:t" `shouldEqual` Right (Tag "abc")
        it "tag strict prefix" $
          runParser P.tqTagS "t:abc;" `shouldEqual` Right (TagStrict "abc")
        it "tag strict suffix" $
          runParser P.tqTagS "abc:t;" `shouldEqual` Right (TagStrict "abc")
        it "uri" $
          runParser P.tqUri "u:http://example.com" `shouldEqual` Right (Url "http://example.com")
        it "text" $
          runParser P.tqText "abc:def" `shouldEqual` Right (AnyText "abc:def")
        it "text strict" $
          runParser P.tqTextS "abc:def;" `shouldEqual` Right (AnyTextStrict "abc:def")
      describe "unit k=2" do
        it "not tag" $
          runParser P.tqNot "!t:abc" `shouldEqual` Right (Not (Tag "abc"))
        it "all one item" $
          runParser P.tqAll "(& t:abc )" `shouldEqual`
          Right (All [Tag "abc"])
        it "all multi item" $
          runParser P.tqAll "(& t:abc ake u:scc )" `shouldEqual`
          Right (All [Tag "abc", AnyText "ake", Url "scc"])
        it "any one item" $
          runParser P.tqAny "(|  u:scc )" `shouldEqual`
          Right (Any [Url "scc"])
        it "any multi item" $
          runParser P.tqAny "(| t:abc u:scc ake )" `shouldEqual`
          Right (Any [Tag "abc", Url "scc", AnyText "ake"])
      describe "various text" do
        it "invalid: start with )" $
          runParser P.tqText ")abc" `shouldNotEqual` Right (AnyText ")abc")
        it "invalid: start with u:)" $
          runParser P.tqUri "u:)abc" `shouldNotEqual` Right (Url ")abc")
        pending "a to de ka ku"
      describe "whole" do
        it "focus tag" $
          runParser parser "t:abc" `shouldEqual` Right (Tag "abc")
        it "ends with whitespace" $
          runParser parser "t:abc " `shouldEqual` Right (Tag "abc")
        it "complex" $
          runParser parser "(& t:abc !u:abc (| t:def cet:t; ) )" `shouldEqual`
          Right (All [Tag "abc", Not (Url "abc"), Any [Tag "def", TagStrict "cet"]])
