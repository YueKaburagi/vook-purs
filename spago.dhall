{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name =
    "vook-purs"
, dependencies =
    [ "affjax"
    , "argonaut-codecs"
    , "argonaut-core"
    , "console"
    , "data-default"
    , "effect"
    , "foldable-traversable"
    , "foreign-object"
    , "halogen"
    , "lazy"
    , "newtype"
    , "ordered-collections"
    , "parsing"
    , "psci-support"
    , "spec"
    , "string-parsers"
    ]
, packages =
    ./packages.dhall
, sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
}
